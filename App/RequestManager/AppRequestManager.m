//
//  AppRequestManager.m
//  App
//
//  Created by saurabh-pc on 10/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import "AppRequestManager.h"
#import "List.h"
#import "ListPhoto.h"
@implementation AppRequestManager
{
    RKObjectMapping *listOM;
    RKObjectMapping *listImgOM;
    RKObjectMapping *listImgOM_2;
}

-(void)BaseManager{
    
    NSURL *baseURL = [NSURL URLWithString:APPBASEURL];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    //[client registerHTTPOperationClass:[AFJSONRequestOperation class]];

    //[client setDefaultHeader:@"sessiontoken" value:[PLSetupDataManager sharedInstance].token];
    // initialize RestKit
     //[client setDefaultHeader:@"Content-type" value:@"application/javascript"];
    //[client setDefaultHeader:@"Content-type" value:RKMIMETypeFormURLEncoded];
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/html"];
   // [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"application/javascript"];

    [RKObjectManager setSharedManager:objectManager];
 MANAGERBASEURL=objectManager;
}
+ (AppRequestManager *)sharedInstance {
    static AppRequestManager *__instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __instance = [[AppRequestManager alloc] init];
    });
    return __instance;
}
-(void)configureObjectManager
{
    //TODO:API API for List_OM
     listOM=[RKObjectMapping mappingForClass:[List class]];
    [listOM addAttributeMappingsFromArray:@[@"title"]];
    [listOM addAttributeMappingsFromDictionary:@{
                                                   @"id": @"imgid",
                                                   
                                                   }];
    
    //TODO:API API for image detail_OM
    //listImgOM=[RKObjectMapping mappingForClass:[List class]];
    listImgOM_2=[RKObjectMapping mappingForClass:[ListPhoto class]];
    /*[listImgOM_2 addAttributeMappingsFromDictionary:@{
                                                 @"label": @"label",@"url": @"url",@"width": @"width",@"height": @"height"
                                                 
                                                 }];*/
   [listImgOM_2 addAttributeMappingsFromArray:@[ @"label", @"url", @"width", @"height",@"source"]];
   // [listImgOM addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"photo" toKeyPath:@"photo" withMapping:listImgOM_2]];
    
    
}
- (void)listImages:(NSString *)param1 andpagecounter:(int)page paramvalue:(NSString *)param2 paramvlue2:(NSString*)param3 anotherparam:(NSString*)param4 callback:(void (^)(NSArray *listarrrsy, NSError *error))callback {
    
    [self BaseManager];
    NSIndexSet *successStatusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:listOM
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:@""
                                                                                           keyPath:@"photos.photo"
                                                                                       statusCodes:successStatusCodes];
    //  [[RKObjectManager sharedManager] addResponseDescriptor:responseDescriptor];
    //2a91464ced1d534f6f0c2e525b71474b
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:APPSEARCHAPI forKey:@"method"];
    [dic setObject:APPAPIKEY forKey:@"api_key"];
    [dic setObject:[self added:param2] forKey:@"text"];
    [dic setObject:@"1" forKey:@"privacy_filter"];
    [dic setObject:[NSNumber numberWithInt:page] forKey:@"page"];
    
    [dic setObject:[NSNumber numberWithInt:APPAPIPERPAGE] forKey:@"per_page"];
    [dic setObject:@"json" forKey:@"format"];
    [dic setObject:@"1" forKey:@"nojsoncallback"];



    [MANAGERBASEURL addResponseDescriptor:responseDescriptor];
    
    [MANAGERBASEURL getObject:nil path:@"" parameters:dic success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [MANAGERBASEURL removeResponseDescriptor:responseDescriptor];
        
        
        NSArray *arrval = mappingResult.array;
        callback(arrval, nil);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [MANAGERBASEURL removeResponseDescriptor:responseDescriptor];
        
        callback(nil, error);
    }];
    
}

- (void)imagelist:(NSString *)param1 callback:(void (^)(NSArray *photoinfio, NSError *error,NSString *param))callback {
    
    NSIndexSet *successStatusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:listImgOM_2
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:@""
                                                                                           keyPath:@"sizes.size"
                                                                                       statusCodes:successStatusCodes];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    // [dic setObject:@"flickr.photos.getInfo" forKey:@"method"];
    [dic setObject:APPSEARCHAPIDETAIL forKey:@"method"];
    [dic setObject:APPAPIKEY forKey:@"api_key"];
    [dic setObject:param1 forKey:@"photo_id"];
    [dic setObject:@"json" forKey:@"format"];
    [dic setObject:@"1" forKey:@"nojsoncallback"];
    NSURL* url = [[NSURL alloc]initWithString:APPBASEURL];
    RKObjectManager* objectManager = [RKObjectManager managerWithBaseURL:url];
   // [objectManager.HTTPClient setAuthorizationHeaderWithUsername:@"username" password:@"password"];
    
    //NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLRequest *request = [objectManager requestWithObject:nil method:RKRequestMethodGET path:@"" parameters:dic];
   // [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/html"];
    [objectManager addResponseDescriptor:responseDescriptor];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[ responseDescriptor ]];
    
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
               NSArray *arrval = mappingResult.array;

        [objectManager removeResponseDescriptor:responseDescriptor];
        RKLogInfo(@"Load collection of Articles: %@", mappingResult.array);
        callback(arrval, nil,param1);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [objectManager removeResponseDescriptor:responseDescriptor];
        RKLogError(@"Operation failed with error: %@", error);
         callback(nil, error,nil);
    }];
    //[objectManager enqueueObjectRequestOperation:objectRequestOperation];
    [objectRequestOperation start];

    
    
}
-(id)added:(NSString*)obj
{
    if (obj.length)
        return obj;
    else
    return [NSNull null];
}

@end
