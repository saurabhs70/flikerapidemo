//
//  AppRequestManager.h
//  App
//
//  Created by saurabh-pc on 10/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "ListPhoto.h"
@interface AppRequestManager : NSObject
{
    RKObjectManager *MANAGERBASEURL;
}

+ (AppRequestManager *)sharedInstance;
-(void)configureObjectManager;

- (void)listImages:(NSString *)param1 andpagecounter:(int)page paramvalue:(NSString *)param2 paramvlue2:(NSString*)param3 anotherparam:(NSString*)param4 callback:(void (^)(NSArray *listarrrsy, NSError *error))callback;
- (void)imagelist:(NSString *)param1 callback:(void (^)(NSArray *photoinfio, NSError *error,NSString *param))callback;
@end
