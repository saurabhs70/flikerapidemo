//
//  ListPhoto.h
//  App
//
//  Created by saurabh-pc on 10/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListPhoto : NSObject
@property (strong,nonatomic) NSString *label;
@property (strong,nonatomic) NSString *url;
@property (strong,nonatomic) NSString *source;
@property double width;
@property double height;
@property (strong,nonatomic) NSString *canblog;


@end
