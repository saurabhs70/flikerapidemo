//
//  List.h
//  App
//
//  Created by saurabh-pc on 10/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListPhoto.h"
@interface List : NSObject

@property (strong,nonatomic) NSString *title;
@property (strong,nonatomic) NSString *imgid;
@property (strong,nonatomic) NSString *imgUrl;
@property (strong,nonatomic) NSArray *photos;
@end
