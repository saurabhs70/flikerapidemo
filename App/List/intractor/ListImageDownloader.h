//
//  ListImageDownloader.h
//  App
//
//  Created by saurabh-pc on 11/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ListImageDownloader : NSObject
+ (ListImageDownloader *)sharedInstance;
-(void)setupAlloc;
@property(strong,nonatomic) NSCache *imageCache;
@property(strong,nonatomic) NSCache *imageCacheDetail;

@property(strong,nonatomic) NSOperationQueue *imgdownloadque;
@property(strong,nonatomic) NSOperationQueue *imgdownloadqueDetail;

- (void)downloadImageWithURL:(NSURL *)url andimgid:(NSString*)imgid andindexpath:(NSIndexPath *)index completionBlock:(void (^)(BOOL succeeded, UIImage *image,NSString *imgid,NSIndexPath *ind))completionBlock;
- (void)downloadDetailImageWithURL:(NSURL *)url andimgid:(NSString*)imgid completionBlock:(void (^)(BOOL succeeded, UIImage *image,NSString *imgid))completionBlock;
@end
