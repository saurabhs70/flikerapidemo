//
//  ListApiManager.h
//  App
//
//  Created by saurabh-pc on 12/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "List.h"
@interface ListApiManager : NSObject
-(void)loadmore:(int)paging andSearchtxt:(NSString*)searchtxt anotherparam:(NSString*)str callback:(void (^)(NSArray *vehiclelocation))callback;

@end
