//
//  ListImageDownloader.m
//  App
//
//  Created by saurabh-pc on 11/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import "ListImageDownloader.h"

@implementation ListImageDownloader
+ (ListImageDownloader *)sharedInstance {
    static ListImageDownloader *__instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __instance = [[ListImageDownloader alloc] init];
    });
    return __instance;
}
-(void)setupAlloc
{
    _imgdownloadque=[[NSOperationQueue alloc]init];
    _imgdownloadqueDetail=[[NSOperationQueue alloc]init];

}
- (void)downloadImageWithURL:(NSURL *)url andimgid:(NSString*)imgid andindexpath:(NSIndexPath *)index completionBlock:(void (^)(BOOL succeeded, UIImage *image,NSString *imgid,NSIndexPath *ind))completionBlock
{
    if ([self maakeOperation:imgid]) {
        
        if ([_imageCache objectForKey:imgid]) {
            NSLog(@"");
        }
        NSOperation *op=[[NSOperation alloc]init];
        op.name=imgid;
        [op setCompletionBlock:^{
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
            
                NSURLSession *session = [NSURLSession sharedSession];
                NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                        completionHandler:
                                              ^(NSData *data, NSURLResponse *response, NSError *error) {
                                                  // ...
                                                  if ( !error )
                                                  {
                                                      UIImage *image = [[UIImage alloc] initWithData:data];
                                                      if (imgid.length&& image!=nil)

                                                      [_imageCache setObject:image forKey:imgid];
                                                      NSLog(@"%lu thread",(unsigned long)[_imgdownloadque operations].count);
                                                      ;
                                                     // [task  cancel];
                                                      completionBlock(YES,image,imgid,index);
                                                      
                                                  }
                                                  else
                                                  {
                                                     // [task  cancel];
                                                      completionBlock(NO,nil,nil,index);

                                                  }
                                              }];
            
                [task resume];
        }];
    [self.imgdownloadque addOperation:op];
    }
//    else
//    {
//        
//        completionBlock(NO,nil,nil);
//
//    }

}
- (void)downloadDetailImageWithURL:(NSURL *)url andimgid:(NSString*)imgid completionBlock:(void (^)(BOOL succeeded, UIImage *image,NSString *imgid))completionBlock
{
    if ([self maakeOperationDetail:imgid]) {
        
        if ([_imageCacheDetail objectForKey:imgid]) {
            NSLog(@"");
        }
        NSOperation *op=[[NSOperation alloc]init];
        op.name=imgid;
        [op setCompletionBlock:^{
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [NSURLRequest
             requestWithURL:url
             cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
             timeoutInterval:30.0f];

            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              // ...
                                              if ( !error )
                                              {
                                                  UIImage *image = [[UIImage alloc] initWithData:data];
                                                  if (imgid.length&& image!=nil)
                                                      [_imageCacheDetail setObject:image forKey:imgid];

                                                
                                                  //NSLog(@"%lu thread",(unsigned long)[_imgdownloadque operations].count);
                                                  ;
                                                  // [task  cancel];
                                                  completionBlock(YES,image,imgid);
                                                  
                                              }
                                              else
                                              {
                                                  // [task  cancel];
                                                  completionBlock(NO,nil,nil);
                                                  
                                              }
                                          }];
            
            [task resume];
        }];
        [self.imgdownloadqueDetail addOperation:op];
    }
    //    else
    //    {
    //
    //        completionBlock(NO,nil,nil);
    //
    //    }
    
}
-(BOOL)maakeOperation:(NSString*)name
{
    BOOL add=true;
    
    for(NSOperation *op in [_imgdownloadque operations])
    {
        if ([op.name isEqualToString:name]) {
            add=false;
            break;
        }
    }
    
    return add;
}
-(BOOL)maakeOperationDetail:(NSString*)name
{
    BOOL add=true;
    
    for(NSOperation *op in [_imgdownloadqueDetail operations])
    {
        if ([op.name isEqualToString:name]) {
            add=false;
            break;
        }
    }
    
    return add;
}

/*
 //         NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
 //            [NSURLConnection sendAsynchronousRequest:request
 //                                               queue:_imgdownloadque
 //                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
 //                                       if ( !error )
 //                                       {
 //                                           UIImage *image = [[UIImage alloc] initWithData:data];
 //                                           [_imageCache setObject:image forKey:imgid];
 //                                           NSLog(@"%lu",(unsigned long)[_imgdownloadque operations].count);
 //
 //                                           completionBlock(YES,image,imgid);
 //
 //                                       } else{
 //                                           NSLog(@"");
 //                                           NSLog(@"%lu",(unsigned long)[_imgdownloadque operations].count);
 //
 //                                           completionBlock(NO,nil,nil);
 //                                       }
 //                                   }];
 ////        }];
 ////        [self.imgdownloadque addOperation:op];
 ////
 ////    }
 ////    else
 ////    {
 ////        completionBlock(NO,nil,nil);
 ////    }
 

 */
@end
