//
//  ViewController.h
//  App
//
//  Created by saurabh-pc on 10/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListCell.h"
#import "List.h"
#import "ListPhoto.h"
#import "ListImageDownloader.h"
#import "ListDetailViewController.h"
#import "ListApiManager.h"
@interface ViewController : UIViewController<UISearchBarDelegate>
{
    NSMutableArray *listArr;
    int pageconter;
    BOOL apicall;
    NSString *searchTxt;
}
@property (weak, nonatomic) IBOutlet UISearchBar *listsearchbar;

@property (weak, nonatomic) IBOutlet UICollectionView *tblList;

@end

