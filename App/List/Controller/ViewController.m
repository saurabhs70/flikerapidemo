//
//  ViewController.m
//  App
//
//  Created by saurabh-pc on 10/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//
//https://api.flickr.com/services/feeds/photos_public.gne?tags=kitten&format=json
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    pageconter=1;
    listArr=[[NSMutableArray alloc]init];
    [ListImageDownloader sharedInstance].imageCache=[[NSCache alloc]init];
    [ListImageDownloader sharedInstance].imageCacheDetail=[[NSCache alloc]init];

   // [self loadmore:pageconter];
    apicall=false;
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title=@"FlikerApp";
}
-(void)setuparray:(NSArray*)listarr
{
    apicall=false;
    [listArr addObjectsFromArray:listarr];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_tblList reloadData];
    });
//    NSLog(@"%@",listarr);
//    for (List *obj in listArr) {
//        if (!obj.imgUrl.length) {
//            NSLog(@"");
//        }
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return listArr.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ListCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"listCellId" forIndexPath:indexPath];
    
    List *obj=[listArr objectAtIndex:indexPath.row];
    NSURL *url=[NSURL URLWithString:obj.imgUrl];
    if ([[ListImageDownloader sharedInstance].imageCache objectForKey:obj.imgid]) {
        cell.imgcell.image=[[ListImageDownloader sharedInstance].imageCache objectForKey:obj.imgid];
    }
    else
    {
    [[ListImageDownloader sharedInstance]downloadImageWithURL:url  andimgid:obj.imgid andindexpath:indexPath completionBlock:^(BOOL succeeded, UIImage *image,NSString *imgid,NSIndexPath *ind) {
       // if (succeeded) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[_tblList reloadData];
                ListCell *blockCell = (ListCell *)[collectionView cellForItemAtIndexPath:ind];
                blockCell.imgcell.image = image;
                NSArray* indexArray = [NSArray arrayWithObjects:ind, nil];
                if (listArr.count>=ind.row) {
                    [self.tblList reloadItemsAtIndexPaths: indexArray];

                }
                //NSLog(@"%@",[ListImageDownloader sharedInstance].imageCache);
            });
        //}


    }];
    }
   // cell.backgroundColor=[UIColor greenColor];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}
//-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
//    
//    if ((indexPath.row>=listArr.count-1)&&!(indexPath.row==0)) {
//        if (apicall!=true) {
//            apicall=true;
//            pageconter=++pageconter;
//            [self loadmore:pageconter andSearchtxt:searchTxt];
//
//        }
//    }
//}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSArray<NSIndexPath*>* visibleCells = [self.tblList indexPathsForVisibleItems];
    NSIndexPath* lastIndexPath = [NSIndexPath indexPathForItem:(listArr.count - 1) inSection:0];
    
    if([visibleCells containsObject:lastIndexPath]) {
        //This means you reached at last of your datasource. and here you can do load more process from server
        if ((lastIndexPath.row>=listArr.count-1)&&!(lastIndexPath.row==0)) {
            if (apicall!=true) {
                apicall=true;
                pageconter=++pageconter;
                [self loadmore:pageconter andSearchtxt:searchTxt];
                
            }
        }
    }
}
-(void)loadmore:(int)paging andSearchtxt:(NSString*)searchtxt
{
    ListApiManager *all=[[ListApiManager alloc]init];
    [all loadmore:paging andSearchtxt:searchtxt anotherparam:nil callback:^(NSArray *vehiclelocation) {
        [self setuparray:vehiclelocation];

    }];

}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    // start a new one in 0.3 seconds
    [self performSelector:@selector(doRemoteQuery:) withObject:searchText afterDelay:0.3];

}
-(void)doRemoteQuery:(NSString*)searchquery
{
    [self resetData:searchquery];
}
-(void)resetData:(NSString*)txtSearch
{
    searchTxt=txtSearch;
    listArr=[[NSMutableArray alloc]init];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_tblList reloadData];
    });

    pageconter=1;
    [self loadmore:pageconter andSearchtxt:searchTxt];

}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self presentDetail:indexPath];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showimageDetail"]) {
        NSIndexPath *path=sender;
      //  NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ListDetailViewController *destViewController = segue.destinationViewController;
        List *obj=[listArr objectAtIndex:path.row];
        destViewController.listarry = obj;
    }
}
-( void)presentDetail:(NSIndexPath*)indexpath
{
    List *obj=[listArr objectAtIndex:indexpath.row];
    ListDetailViewController *add =[[GlobalObject sharedInstance]getViewControllerByName:APPLISTDETAILVIEWCONTROLLER];
    add.listarry=obj;
        [self addToNavigationControllerViewController:add];
   

    
}
- (void)addToNavigationControllerViewController:(UIViewController *)controller
{    CGRect screenRect = [[UIScreen mainScreen] bounds];

    [controller willMoveToParentViewController:self.navigationController];
    [self.navigationController addChildViewController:controller];
    controller.view.frame =CGRectMake(0, 65, screenRect.size.width, screenRect.size.height); //self.navigationController.view.bounds;
    [self.navigationController.view addSubview:controller.view];
    //self.navigationItem.title=@"po";
    [controller didMoveToParentViewController:self.navigationController];
}
@end
