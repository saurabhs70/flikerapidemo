//
//  GlobalObject.m
//  App
//
//  Created by saurabh-pc on 12/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import "GlobalObject.h"

@implementation GlobalObject
+ (GlobalObject *)sharedInstance
{
    static GlobalObject *__instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __instance = [[GlobalObject alloc] init];
    });
    return __instance;

}
-(id)getViewControllerByName:(NSString*)name
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:APPSTORYBOARD
                                                         bundle:nil];
    UIViewController *add =
    [storyboard instantiateViewControllerWithIdentifier:name];
    return add;
}
@end
