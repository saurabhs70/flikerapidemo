//
//  GlobalObject.h
//  App
//
//  Created by saurabh-pc on 12/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GlobalObject : NSObject
+ (GlobalObject *)sharedInstance;
-(id)getViewControllerByName:(NSString*)name;
@end
