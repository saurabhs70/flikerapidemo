//
//  Macro.h
//  App
//
//  Created by saurabh-pc on 11/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#ifndef Macro_h
#define Macro_h
//TODO:APP Identifier
#define APPSTORYBOARD @"Main"
#define APPLISTVIEWCONTROLLER @"viewControllerId"
#define APPLISTDETAILVIEWCONTROLLER @"ListDetailViewControllerId"
//TODO:App Api
#define APPBASEURL @"https://api.flickr.com/services/rest/"
//TODO:API Method
#define APPAPIKEY @"2a91464ced1d534f6f0c2e525b71474b"
#define APPSEARCHAPI @"flickr.photos.search"
#define APPSEARCHAPIDETAIL @"flickr.photos.getSizes"
#define APPAPIPERPAGE 15
#endif /* Macro_h */
