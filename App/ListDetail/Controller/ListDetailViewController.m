//
//  ListDetailViewController.m
//  App
//
//  Created by saurabh-pc on 11/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import "ListDetailViewController.h"

@interface ListDetailViewController ()

@end

@implementation ListDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadGesture];
    [self addSubviewWithBounce:self.img];
   // [self performSelector:@selector(subscribe) withObject:self afterDelay:5.0 ];
    if ([[ListImageDownloader sharedInstance].imageCacheDetail objectForKey:_listarry.imgid]) {
        _img.image=[[ListImageDownloader sharedInstance].imageCacheDetail objectForKey:_listarry.imgid];
    }
    else
    {
        if (_listarry.photos.count>5) {
            
        
        ListPhoto *obj=[_listarry.photos objectAtIndex:5];
        NSURL *url=[NSURL URLWithString:obj.source];
        [[ListImageDownloader sharedInstance] downloadDetailImageWithURL:url andimgid:_listarry.imgid completionBlock:^(BOOL succeeded, UIImage *image, NSString *imgid) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _img.image=image;
            });
        }];
        }
    }
}
-(void)addSubviewWithBounce:(UIView*)theView
{
    theView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    [self.view addSubview:theView];
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        theView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            theView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                theView.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
}
-(void) subscribe
{
    [self containerRemoveChildViewController:self];
}
- (void)attachToViewController:(UIViewController *)vc withFrame:(CGRect)frame
{
    [vc addChildViewController:self];
    self.view.frame = frame;
    [vc.view addSubview:self.view];
    [self didMoveToParentViewController:vc];
    //[self.navigationController.view setNeedsLayout];
    
}
- (void)containerRemoveChildViewController:(UIViewController *)childViewController {
    
    [childViewController willMoveToParentViewController:nil];
    [childViewController.view removeFromSuperview];
    [childViewController removeFromParentViewController];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)loadGesture
{
    _img.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    
    tapGesture1.numberOfTapsRequired = 1;
    
    [tapGesture1 setDelegate:self];
    
    [_img addGestureRecognizer:tapGesture1];

}
- (void) tapGesture: (id)sender
{
    //handle Tap...
    [self subscribe];
}
@end
