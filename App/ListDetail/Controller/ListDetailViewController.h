//
//  ListDetailViewController.h
//  App
//
//  Created by saurabh-pc on 11/10/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "List.h"
#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)

@protocol CaptureCameradelegate<NSObject>
-(void)Capturedimage:(UIImage*)image;
@end

@interface ListDetailViewController : UIViewController<UIGestureRecognizerDelegate>
{
    BOOL animating;

}
@property (strong,nonatomic) List *listarry;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property(nonatomic,weak)id<CaptureCameradelegate>captureCameradelegate;
- (void)attachToViewController:(UIViewController *)vc withFrame:(CGRect)frame
;
- (void)containerRemoveChildViewController:(UIViewController *)childViewController;
@end
