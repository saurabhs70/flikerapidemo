
### What is this repository for? ###

* Fliker Api demo
* NSOperationqueue example
* image cacheing
* Animation view controller custom Transition 
* Orm mapping



### How do I get set up for Fliker api? ###


* Fliker Api implimentation 
*[STEP1](https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=YOUR_KEY&text=sachin&privacy_filter=1&per_page=10&format=json&nojsoncallback=1)

*[STEP2](https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=YOUR_KEY&photo_id=36900135834&format=json&nojsoncallback=1)

*[step3](https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=YOUR_KEY&photo_id=37566814376&format=json&nojsoncallback=1)

